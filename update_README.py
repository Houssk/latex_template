import os 

with open("doc/head.md", "r") as f:
    head = f.read()

gitlabPage_name = "https://symmehub.gitlab.io/ressources_doctorants/latex_template/"

if not gitlabPage_name.endswith("/"):
    gitlabPage_name+= "/"

prev_text=head+"===============================================\r\n"\
            "# Example links\r\n\r\n"

new_text = ""

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Add thesis
new_text += "## Thesis\r\n"
resdir = "./thesis/fichiers_latex/Manuscrit/"
extension = ".tex"
pdf_names = []
for root, dirs, files in os.walk(resdir, topdown=False):
    for f in files:
        if f.endswith(extension) and f == "Manuscrit.tex":
            pdf_names.append(f[:-len(extension)]+".pdf")

for files in pdf_names:
    new_text += "- [{0}]({1}/{0})\r\n".format(files, gitlabPage_name)

full_text = prev_text + new_text

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
#  Add reports
new_text += "## Reports\r\n"
resdir = "./reports/"
extension = ".tex"
pdf_names = []
for root, dirs, files in os.walk(resdir, topdown=False):
    for f in files:
        if f.endswith(extension):
            pdf_names.append(f[:-len(extension)]+".pdf")

# new_text = ""
for files in sorted(pdf_names):

    new_text += "- [{0}]({1}/{0})\r\n".format(files, gitlabPage_name)

full_text = prev_text + new_text

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
#  Add presentations
new_text += "## Presentations\r\n"
resdir = "./presentations/"
extension = ".tex"
pdf_names = []
for root, dirs, files in os.walk(resdir, topdown=False):
    for f in files:
        if f.endswith(extension):
            pdf_names.append(f[:-len(extension)]+".pdf")

# new_text = ""
for files in sorted(pdf_names):
    if files.startswith("PRES"):
        new_text += "- [{0}]({1}/{0})\r\n".format(files, gitlabPage_name)

full_text = prev_text + new_text

with open('README.md', 'w') as f:
    f.write(full_text)

    
print("READ.md links updated !")
